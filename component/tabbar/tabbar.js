// tabBarComponent/tabBar.js
const app = getApp();
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    tabbar: {
      type: Object,
      value: {
        "backgroundColor": "#ffffff",
        "color": "#979795",
        "selectedColor": "#1c1c1b",
        // "list": wx.getStorageSync('tabBar')
        "list":
        [
          {
            "pagePath": "/pages/teacher/sign_home",
            "text": "首页",
            "iconPath": "../../images/home1.png",
            "selectedIconPath": "../../images/home2.png"
          },
          {
            "pagePath": "/pages/teacher/sign_launch",
            "isSpecial": true,
            "text": "创建签到",
            "iconPath": "../../images/publish.png",
            "selectedIconPath": "../../images/publish.png",
          },
          {
            "pagePath": "/pages/teacher/sign_my",
            "text": "我的",
            "iconPath": "../../images/my1.png",
            "selectedIconPath": "../../images/my2.png"
          }
        ]
      }
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    // isIphoneX: app.globalData.systemInfo.model == "iPhone X" ? true : false,
  },
  /**
   * 组件的方法列表
   */
  methods: {
    shareTherelease(e) {
      if(wx.getStorageSync('userinfo').type==null){
        wx.showModal({
          title: '提示',
          content: "请先在 我的页面内 登录！",//提示内容
          confirmColor: '#0ABF53',//确定按钮的颜色
           success (res) {
          if (res.confirm) {
             wx.switchTab({
               url: '/pages/teacher/sign_my',
             })
           } else if (res.cancel) {
           }
         }
        })
      }else{
        wx.navigateTo({
          url: e.currentTarget.dataset.url,
        })
      }
    },
    onSwitchTab(e){
      wx.switchTab({
        url: e.currentTarget.dataset.url,
      })
    }
  }
})

