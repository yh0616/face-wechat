const app = getApp();
var util = require('../../utils/util.js');
Page({

  data: {
    scrolls: ['功能建议','性能问题','Bug反馈'],
    idx: 0,
    tempFilePaths: '',
    contentValue: '',
    relationValue: '',
  },

  

  //标签选择
  goIndex: function (e) {
   this.setData({
     idx:e.currentTarget.dataset.key
   })
  },

  //拍照
  img_w_show() {
    var _this = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success(res) {
        // tempFilePath可以作为img标签的src属性显示图片
        _this.setData({
          tempFilePaths: res.tempFilePaths
        })
      }
    })
  },

  //删除图片
  deleteImage: function (e) {
    var that = this;
    wx.showModal({
      title: '提示',
      content: '确定要删除此图片吗？',
      success: function (res) {
        if (res.confirm) {
          that.setData({
            tempFilePaths: ""
          })
        } else if (res.cancel) {
        }
      }
    })
  },

  //反馈内容
  contentValue: function (e) {
    this.data.contentValue = e.detail.value
  },

  //联系方式
  relationValue: function (e) {
    this.data.relationValue = e.detail.value
  },

  //提交
  submit: function () {
    var that = this;
    if (that.data.tempFilePaths == '' || that.data.scrolls[that.data.idx] == '' || that.data.contentValue == '' || that.data.relationValue == ''){
      wx.showModal({
        title: '提示',
        content: '请完善信息',
        success (res) {
          if (res.confirm) {
          } else if (res.cancel) {
          }
        }
      })
    }else {
      wx.uploadFile({
        url: 'https://www.xqzjgsu.xyz/fss/auth/sendMail',
        filePath: that.data.tempFilePaths[0],
        name: 'image',
        header: {
          'content-type': 'multipart/form-data',
          'Authorization': 'Bearar ' + wx.getStorageSync('token')
        },
        formData: {
          subject: that.data.scrolls[that.data.idx],
          content: that.data.contentValue + '联系方式：' + that.data.relationValue
        },
        success: function (res) {
          wx.showToast({
            title: '提交成功',
            icon: 'success',
            duration: 1500
          })
          setTimeout(function(){
            wx.navigateBack({
              delta: 1,
            })
          },1500)
        },
        fail: function(res){
          wx.showToast({
            title: '提交失败',
            icon: 'loading',
            duration: 1500
          })
        }
      })
    }
  }
})