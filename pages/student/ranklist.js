// pages/student/ranklist.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tableHeader: [{
        prop: 'rankid',
        width: 200,
        label: '排行'
      },
      {
        prop: 'name',
        width: 200,
        label: '昵称'
      },
      {
        prop: 'signtimes',
        width: 300,
        label: '签到次数'
      }
    ],
    stripe: true,
    border: true,
    outBorder: true,
    msg: '暂无数据',
    TabCur: 0,

    listdata: [], //数据
    moredata: '',
    pageNum: 0, //当前分页；默认第一页
    pageSize: 20,

    scoreListdata: [], //数据
    scoreMoredata: '',
    scorePageNum: 0, //当前分页；默认第一页
    scorePageSize: 20,
  },

  /** 
   * 点击表格一行
   */
  onRowClick: function (e) {},

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      uid: wx.getStorageSync('userinfo').uid,
      listdata: [],
      scoreListdata: [],
    })
    this.loadRankMore();
    this.loadScoreRankMore();
    // this.selectRank()
    this.selectFriendRank()
  },

  // 查询好友榜
  async selectFriendRank() {
    var data = {
      user_id: this.data.uid
    }
    await wx.User.selectFriendRank({
      data: data
    })
    var res = wx.getStorageSync('friendRank')
    if (res.code == 200) {
      this.setData({
        row: res.data
      })
    }
  },

  tabSelect(e) {
    this.setData({
      TabCur: e.currentTarget.dataset.id,
      scrollLeft: (e.currentTarget.dataset.id - 1) * 60
    })
  },

  //发起请求
  loadRankMore: async function () {
    //加载提示
    wx.showLoading({
      title: '加载中',
    })
    var that = this;
    //页面累加
    ++that.data.pageNum;
    await wx.User.selectFriendTotalRank({
      data: {
        pageSize: that.data.pageSize,
        pageNum: that.data.pageNum
      }
    })
    //隐藏加载提示
    wx.hideLoading();
    //判断数据是否为空
    if (wx.getStorageSync('friendTotalRank').Rank != []) {
      console.log("js中的Rank", wx.getStorageSync('friendTotalRank').Rank)
      that.setData({
        //把新加载的数据追加到原有的数组
        listdata: that.data.listdata.concat(wx.getStorageSync('friendTotalRank').Rank), //加载数据
        moredata: wx.getStorageSync('friendTotalRank').Rank,
        pageSize: that.data.pageSize,
        pageNum: that.data.pageNum
      })
    } else {}
  },

  
  //积分发起请求
  loadScoreRankMore: async function () {
    //加载提示
    wx.showLoading({
      title: '加载中',
    })
    var that = this;
    //页面累加
    ++that.data.scorePageNum;
    await wx.User.selectscoreRank({
      data: {
        pageSize: that.data.scorePageSize,
        pageNum: that.data.scorePageNum
      }
    })
    //隐藏加载提示
    wx.hideLoading();
    //判断数据是否为空
    if (wx.getStorageSync('scoreTotalRank').Rank != []) {
      console.log("积分",wx.getStorageSync('scoreTotalRank').Rank)
      that.setData({
        //把新加载的数据追加到原有的数组
        scoreListdata: that.data.scoreListdata.concat(wx.getStorageSync('scoreTotalRank').Rank), //加载数据
        scoreMoredata: wx.getStorageSync('scoreTotalRank').Rank,
        scorePageNum: that.data.scorePageNum,
        scorePageSize: that.data.scorePageSize
      })
    } else {}
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  loadMore: function () {
    var that = this;
    //检查是否还有数据可以加载
    var moredata = that.data.moredata;
    //如果还有，则继续加载
    if (moredata.length != 0) {
      this.loadRankMore();
    } else {
      //如果没有了，则停止加载，显示没有更多内容了
      wx.showToast({
        title: '没有更多了~',
        icon: 'none'
      })
    }
  },
  scoreLoadMore: function () {
    var that = this;
    //检查是否还有数据可以加载
    var moredata = that.data.scoreMoredata;
    //如果还有，则继续加载
    if (moredata.length != 0) {
      this.loadScoreRankMore();
    } else {
      //如果没有了，则停止加载，显示没有更多内容了
      wx.showToast({
        title: '没有更多了~',
        icon: 'none'
      })
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    var uid = this.data.uid;
    if (res.from === 'button') {} else if (res.from === 'menu') {}
    return {
      title: '邀请你成为 ' + wx.getStorageSync('userinfo').name + ' 的好友', // 转发标题
      path: '/pages/student/friendlist?user_id=' + uid, // 当前页面 path ，必须是以 / 开头的完整路径
      imageUrl: 'http://pic.51yuansu.com/pic3/cover/03/80/88/5c078a9366674_610.jpg!/fw/260/quality/90/unsharp/true/compress/true'
    }
  },
})