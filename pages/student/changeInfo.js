// pages/student/changeInfo.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userinfo: wx.getStorageSync('userinfo'),
    // 个人信息
    infoArray: {
      name: '',
      major: '',
      Class: ''
    }
  },
  nameIn: function (e) {
    this.data.infoArray.name = e.detail.value
  },
  majorIn: function (e) {
    this.data.infoArray.major = e.detail.value
  },
  ClassIn: function (e) {
    this.data.infoArray.Class = e.detail.value
  },
  // 提交修改信息
  onSubmit: async function () {
    await wx.User.updateUserInfo({
      data: {
        uid: this.data.userinfo.uid,
        name: this.data.infoArray.name,
        type: this.data.userinfo.type,
        major: this.data.infoArray.major,
        uclass: this.data.infoArray.Class
      }
    })
    wx.navigateBack({
      delta: 1,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      infoArray: JSON.parse(options.info),
      userinfo:wx.getStorageSync('userinfo')
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})