const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userinfo: '',
    cid: ''
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      user_id: options.user_id
    })
  },

  getUserProfile(e) {
    var that = this
    wx.login({
      success: res => {
        wx.User.login({
          url: "/auth/login",
          data: {
            code: res.code,
            userAvatar: wx.getStorageSync('avatarInfo').avatarUrl,
            userAlias:wx.getStorageSync('avatarInfo').nickName
          }
        })
        // 后端用户信息
        var user_id = that.data.user_id
        var data = {
          user_id: user_id,
          friend_id: wx.getStorageSync('userinfo').uid
        }
        wx.User.joinFriend({
          data: data
        })
        setTimeout(
          function () {
            wx.redirectTo({
              url: '../guide/guide',
            })
          },
          1500)
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },


  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})