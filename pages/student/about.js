// pages/about/about.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    appVersion: '',
    uid: wx.getStorageSync('userinfo').uid,
  },

  onLoad: function (options) {
    const accountInfo = wx.getAccountInfoSync();
    this.setData({
      uid: wx.getStorageSync('userinfo').uid,
      appVersion: "1.2.10",
    })
  },

  //帮助
  toHelp: async function () {
    wx.navigateTo({
      url: '../help/help',
    })

  },
  //意见反馈
  feedback: function () {
    wx.navigateTo({
      url: '../feedback/feedback',
    })
  },
  //隐私政策
  privacyPolicy: function () {
    wx.navigateTo({
      url: '../privacy/privacy',
    })
  },

  // 人脸更新
  async updateUserFace() {
    var faceExistFlag = wx.getStorageSync('checkFaceExist').data == 0 ? false : true
    var infoArray = this.data.infoArray
    // 未注册人脸
    if (!faceExistFlag) {
      if (infoArray.name == '') {
        wx.navigateTo({
          url: '/pages/register/register',
        })
      } else {
        wx.navigateTo({
          url: '/pages/student/recognition?flag=true',
        })
      }
    } else {
      // 两个月注册一次
      await wx.User.canUpdateFace({
        data: {
          uid: this.data.uid
        }
      })
      var res = wx.getStorageSync('checkData')
      if (res.code == 200) {
        wx.navigateTo({
          url: '/pages/student/recognition?flag=true',
        })
      } else {
        wx.showToast({
          title: res.message,
          icon: 'none'
        })
      }
    }

  },

})