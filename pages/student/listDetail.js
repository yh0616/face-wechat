// pages/student/listDetail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    lid: '',
    title: '',
    result_len: 0,
    nosign_len:0,
    signtitle: '',
    sign_userList: [],
    currentTab:0,
    unsignUserList:[],
    unsigntip:true,
  },
  //滑动切换
  swiperTab: function (e) {
    var that = this;
    that.setData({
      currentTba: e.detail.current
    });
  },
  //点击切换
  clickTab: function (e) {
    var that = this;
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.target.dataset.current
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad:async function (options) {
    this.setData({
      lid: options.lid,
      title: options.title
    })
    await this.getSign()
    this.setData({
      result_len:this.data.sign_userList.length
    })
    await this.getUnsign()
    this.setData({
      nosign_len:this.data.unsignUserList.length
    })
  },
  // 返回已签到结果
  async getSign() {
    const {
      data: res
    } = await wx.$http.get('/rec/resultTeacher', {
      lid: this.data.lid
    }, {
      token: true,
      checkToken: true
    })
    if (res.code != 200) {
      wx.showToast({
        title: res.message,
        icon: "none"
      })
    } else {
      this.setData({
        signtitle: res.data[0],
        sign_userList: res.data[2]
      })
    }

  },
  // 返回未签到结果
  async getUnsign() {
    const {
			data: res
		} = await wx.$http.get('/rec/noSignInTea', {
      lid: this.data.lid
    }, {
			token: true,
			checkToken: true
		})
    // res.code=197 无名单
		if (res.code != 200) {
			// wx.showToast({
			// 	title: res.message,
			// 	icon: "none"
      // })
      if(res.code == 197){
        this.setData({
          unsigntip:false
        })
      }
		} else {
			this.setData({
        unsignUserList: res.data[2][0]=='null'?[]:res.data[2]
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})