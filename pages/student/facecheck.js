var app = getApp();
Page({
  data: {
    src: "",
    fengmian: "",
    videoSrc: "",
    who: "",
    openid: "",
    token: "",
    windowWidth: 0,
    trackshow: "进行人脸追踪",
    canvasshow: true,
    access_token: ''
  },

  onLoad() {
    var that = this
    wx.showLoading({
      title: '努力加载中',
      mask: true
    })
    //屏幕宽度
    var sysInfo = wx.getSystemInfoSync()
    that.setData({
      windowWidth: sysInfo.windowWidth,
    })
    that.ctx = wx.createCameraContext()
    that.setData({
      openid: app.globalData.openid,
      token: app.globalData.token
    });

    // 每次更新access_token
    wx.request({
      url: "https://aip.baidubce.com/oauth/2.0/token?grant_type=client_credentials&client_id=" + "WLBvQb1doGBGdowv61UPZgOK" + "&client_secret=" + "4y6ASZmVCfxMZsbiPGcsUGua8buLZhmF",
      method: 'POST',
      dataType: "json",
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        that.setData({
          access_token: res.data.access_token
        });
        that.search()
      }
    })
    wx.hideLoading()

    // setTimeout(function () {

    // }, 1000);





    // setTimeout({
    //   that.search(),
    // },2000)



  },

  onReady: function () {},

  track(e) {
    var that = this
    if (e.target.dataset.trackshow == "进行人脸追踪") {
      that.setData({
        trackshow: "停止人脸追踪",
        canvasshow: true
      })
      that.takePhoto()
      that.interval = setInterval(this.takePhoto, 500)
    } else {
      clearInterval(that.interval)
      that.setData({
        trackshow: "进行人脸追踪",
        canvasshow: false
      })
    }
  },

  takePhoto() {
    var that = this
    var takephonewidth
    var takephoneheight
    that.ctx.takePhoto({
      quality: 'low',
      success: (res) => {
        // 获取图片真实宽高
        wx.getImageInfo({
          src: res.tempImagePath,
          success: function (res) {
            takephonewidth = res.width,
              takephoneheight = res.height
          }
        })
        wx.getFileSystemManager().readFile({
          filePath: res.tempImagePath, //选择图片返回的相对路径
          encoding: 'base64', //编码格式
          success: res => { //成功的回调
            wx.request({
              url: "https://aip.baidubce.com/rest/2.0/face/v3/detect?access_token=" + that.data.access_token,
              data: {
                image: res.data,
                image_type: "BASE64",
                max_face_num: 10
              },
              method: 'POST',
              dataType: "json",
              header: {
                'content-type': 'application/json'
              },
              success: function (res) {
                if (res.data.error_code === 0) {
                  var ctx = wx.createContext()
                  ctx.setStrokeStyle('#31859c')
                  ctx.lineWidth = 3
                  for (let j = 0; j < res.data.result.face_num; j++) {
                    var cavansl = res.data.result.face_list[j].location.left / takephonewidth * that.data.windowWidth
                    var cavanst = res.data.result.face_list[j].location.top / takephoneheight * that.data.windowWidth
                    var cavansw = res.data.result.face_list[j].location.width / takephonewidth * that.data.windowWidth
                    var cavansh = res.data.result.face_list[j].location.height / takephoneheight * that.data.windowWidth
                    ctx.strokeRect(cavansl, cavanst, cavansw, cavansh)
                  }
                  wx.drawCanvas({
                    canvasId: 'canvas',
                    actions: ctx.getActions()
                  })
                } else {
                  var ctx = wx.createContext()
                  ctx.setStrokeStyle('#31859c')
                  ctx.lineWidth = 3
                  wx.drawCanvas({
                    canvasId: 'canvas',
                    actions: ctx.getActions()
                  })
                }
              },
            })

          }
        })
      }
    })
  },

  search() {
    var that = this
    that.setData({
      who: ""
    })
    var takephonewidth
    var takephoneheight
    that.ctx.takePhoto({
      quality: 'heigh',
      success: (res) => {
        // 获取图片真实宽高
        wx.getImageInfo({
          src: res.tempImagePath,
          success: function (res) {
            takephonewidth = res.width,
              takephoneheight = res.height
          }
        })
        that.setData({
            src: res.tempImagePath
          }),
          wx.getFileSystemManager().readFile({
            filePath: that.data.src, //选择图片返回的相对路径
            encoding: 'base64', //编码格式
            success: res => {
              wx.request({
                url: "https://aip.baidubce.com/rest/2.0/face/v3/multi-search?access_token=" + that.data.access_token,
                data: {
                  image: res.data,
                  image_type: "BASE64",
                  group_id_list: "signpeople",
                  max_face_num: 10,
                  match_threshold: 60,

                },
                method: 'POST',
                dataType: "json",
                header: {
                  'content-type': 'application/json'
                },
                success: function (res) {
                  if (res.data.result.face_list[0].user_list[0].score > 80) {
                    wx.navigateTo({
                      url: 'home',
                    })
                  }

                  var ctx = wx.createContext()
                  if (res.data.error_code === 0) {
                    ctx.setStrokeStyle('#31859c')
                    ctx.setFillStyle('#31859c');
                    ctx.lineWidth = 3
                    for (let j = 0; j < res.data.result.face_num; j++) {
                      var cavansl = res.data.result.face_list[j].location.left / takephonewidth * that.data.windowWidth / 2
                      var cavanst = res.data.result.face_list[j].location.top / takephoneheight * that.data.windowWidth / 2
                      var cavansw = res.data.result.face_list[j].location.width / takephonewidth * that.data.windowWidth / 2
                      var cavansh = res.data.result.face_list[j].location.height / takephoneheight * that.data.windowWidth / 2
                      var cavanstext = res.data.result.face_list[j].user_list.length > 0 ? res.data.result.face_list[j].user_list[0].user_id + " " + res.data.result.face_list[j].user_list[0].score.toFixed(0) + "%" : "Unknow"
                      ctx.setFontSize(14);
                      ctx.fillText(cavanstext, cavansl, cavanst - 2)
                      ctx.strokeRect(cavansl, cavanst, cavansw, cavansh)
                    }
                    wx.drawCanvas({
                      canvasId: 'canvasresult',
                      actions: ctx.getActions()
                    })
                  } else {
                    that.setData({
                      who: res.data.error_msg
                    })
                    var ctx = wx.createContext()
                    ctx.setStrokeStyle('#31859c')
                    ctx.lineWidth = 3
                    wx.drawCanvas({
                      canvasId: 'canvasresult',
                      actions: ctx.getActions()
                    })
                  }
                },
              })
            }
          })
      }
    })

  },

  startRecord() {
    this.ctx.startRecord({
      success: (res) => {
      },
    })
  },
  stopRecord() {
    this.ctx.stopRecord({
      success: (res) => {
        this.setData({
          fengmian: res.tempThumbPath,
          videoSrc: res.tempVideoPath
        })
      }
    })
  },
  uploadRecord() {
    var that = this;
    wx.showLoading({
      title: '上传中',
    })
    //获取摄像头信息
    wx.request({
      url: app.globalData.urlHeader + '/login/cameralist',
      data: {
        openid: app.globalData.openid,
        token: app.globalData.token
      },
      method: 'POST',
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        if (res.data.code === 0) {
          if (res.data.data.cameras == null) {
            wx.request({
              url: app.globalData.urlHeader + '/login/addcamera',
              data: {
                openid: app.globalData.openid,
                token: app.globalData.token,
                camera: "phone",
              },
              method: 'POST',
              header: {
                'content-type': 'application/json'
              },
              success: function (res) {
                if (res.data.code === 0) {
                } else {
                }
              }
            })
          } else {
            var cameras = res.data.data.cameras
            if (cameras.includes("phone")) {
              return false
            } else {
              wx.request({
                url: app.globalData.urlHeader + '/login/addcamera',
                data: {
                  openid: app.globalData.openid,
                  token: app.globalData.token,
                  camera: "phone"
                },
                method: 'POST',
                header: {
                  'content-type': 'application/json'
                },
                success: function (res) {
                  if (res.data.code === 0) {
                  } else {
                  }
                }
              })
            }
          }
        } else {
          wx.hideLoading()
          wx.showToast({
            title: '获取摄像头列表失败！',
            image: '../../img/about.png',
            duration: 1000
          })

        }
      }
    })

    wx.uploadFile({
      url: app.globalData.urlHeader + '/upload',
      filePath: that.data.videoSrc,
      name: 'file',
      formData: {
        'cameraid': 'phone',
        'openid': app.globalData.openid,
        'token': app.globalData.token,
        'tag': 2
      },
      success: function (res) {
        var result = JSON.parse(res.data).data.filename
        wx.uploadFile({
          url: app.globalData.urlHeader + '/upload/fengmian',
          filePath: that.data.fengmian,
          name: 'file',
          formData: {
            'openid': app.globalData.openid,
            'token': app.globalData.token,
            'name': result
          },
          success(res) {
            that.setData({
                fengmian: "",
                videoSrc: ""
              }),
              wx.hideLoading()
            wx.showToast({
              title: '上传成功',
              icon: 'success',
              duration: 2000
            })
            setTimeout(function () {
              wx.switchTab({
                url: '../index/index'
              })
            }, 2000)

          },
          fail(res) {
            wx.hideLoading()
            wx.showToast({
              title: '上传失败',
              image: '../../img/about.png',
              duration: 2000
            })

          }
        })
      },
      fail(res) {
        wx.hideLoading()
        wx.showToast({
          title: '上传失败',
          image: '../../img/about.png',
          duration: 2000
        })

      }

    })
  },

  onUnload: function () {
    var that = this
    clearInterval(that.interval)
  },

  error(e) {
  }

})