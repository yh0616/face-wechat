// pages/student/recognition.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isAuth: false,
    src: '',
    img: false,
    uid: wx.getStorageSync('userinfo').uid,
    imgPath: '',
    resCode:0,

    navConfig: {
      title: '确认框',
      type: 1,
      config: {
        show: true,
        type: 'confirm',
        showTitle: true,
        title: '提示',
        content: '这个是确认框',
        confirmText: '确定',
        confirmColor: '#3683d6',
        cancelText: '取消',
        cancelColor: '#999'
      }
    },

    navflag: false
  },
  // 打开授权设置界面
  openSetting() {
    const _this = this
    let promise = new Promise((resolve, reject) => {
      wx.showModal({
        title: '授权',
        content: '请先授权获取摄像头权限',
        success(res) {
          if (res.confirm) {
            wx.openSetting({
              success(res) {
                if (res.authSetting['scope.camera']) { // 用户打开了授权开关
                  resolve(true)
                } else { // 用户没有打开授权开关， 继续打开设置页面
                  _this.openSetting().then(res => {
                    resolve(true)
                  })
                }
              },
              fail(res) {}
            })
          } else if (res.cancel) {
            _this.openSetting().then(res => {
              resolve(true)
            })
          }
        }
      })
    })
    return promise;
  },

  skip() {
    wx.redirectTo({
      url: '/pages/student/home',
    })
  },

  takePhoto() {
    const ctx = wx.createCameraContext()
    ctx.takePhoto({
      quality: 'high',
      success: (res) => {
        this.setData({
          src: res.tempImagePath,
          img: true
        })
        wx.previewImage({
          current: res.tempImagePath, // 当前显示图片的http链接
          urls: [res.tempImagePath] // 需要预览的图片http链接列表
        })
      }
    })
  },
  secondTakePhoto() {
    this.setData({
      src: '',
      img: false
    })
  },
  finish: function () {
    wx.showLoading({
      title: '识别中...',
    })
    var that = this
    wx.uploadFile({
      url: 'https://www.xqzjgsu.xyz/fss/auth/addFace', 
      filePath: that.data.src,
      name: 'file',
      header: {
        'content-type': 'multipart/form-data',
        'Authorization': 'Bearar ' + wx.getStorageSync('token')
      },
      formData: {
        'uid': that.data.uid,
      },
      success: async function (res) {
        var result = JSON.parse(res.data)
        var navTemp = that.data.navConfig
        navTemp.config.content = result.message
        that.setData({
          navConfig: navTemp,
          resCode:result.code
        })
        wx.hideLoading({
          success: (res) => {},
        })
        that.onShowDioTap()
      }
    })
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      uid: wx.getStorageSync('userinfo').uid
    })
    const _this = this
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.camera']) {
          // 用户已经授权
          _this.setData({
            isAuth: true
          })
        } else {
          // 用户还没有授权，向用户发起授权请求
          wx.authorize({
            scope: 'scope.camera',
            success() { // 用户同意授权
              _this.setData({
                isAuth: true
              })
            },
            fail() { // 用户不同意授权
              _this.openSetting().then(res => {
                _this.setData({
                  isAuth: true
                })
              })
            }
          })
        }
      },
      fail: res => {}
    })

    var that = this
    // 进入此页面的：
    // 1.补录，会传一个特殊的参数，有则进行
    if (options.flag=='true') {
      that.setData({
        navflag: true
      })
      this.info()
    }
    // 2.第一次选择学生角色
    else {
      wx.showModal({
        title: '提示',
        content: '确认人脸注册？选择取消，将不会注册人脸，同时无法作为签到者签到！',
        success(res) {
          if (res.confirm) {
            that.setData({
              navflag: true
            })
            that.info()
          } else if (res.cancel) { 
            wx.redirectTo({
              url: '/pages/student/home',
            })
          }
        }
      })
      
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  info() {
    if(this.data.navflag) {
      var navTemp = this.data.navConfig
      navTemp.config.content = '人脸注册只会保存您的特征数据，不会保存您的照片，特征数据只会用于签到时的人脸比对，不会用于任何其他非授权场景！'
      this.setData({
        navConfig: navTemp,
        faceflag: true
      })
      this.onShowDioTap()
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.hideHomeButton();
  },

  // 显示 dio
  onShowDioTap() {
    const type = 1;
    const config = JSON.parse(JSON.stringify(this.data.navConfig.config));
    this.setData({
      currentConf: config,
      type
    });
  },

  // 确定按钮
  onConfirmTap() {  
    if(this.data.resCode == 200){
      wx.redirectTo({
        url: '/pages/student/home',
      })
    }
  },

  // 取消按钮
  onCancelTap() {
    wx.redirectTo({
      url: '/pages/student/home',
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})