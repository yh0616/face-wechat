// pages/dailyTask/dailyTask.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    dailyTask:'',
    onDisabled:false,
    onDisabled2:false,
  },
  onLoad: function (options) {
    // 每日任务
    this.dailyTask()
  },
  // 每日任务
  dailyTask:async function() {
    await wx.User.dailyTask({
      data:{
        uid: wx.getStorageSync('userinfo').uid,
      }
    })
    this.setData({
      dailyTask:wx.getStorageSync('dailyTask'),
      
    })
    this.setData({
      onDisabled:this.data.dailyTask.task1button=='已完成'?true:false,
      onDisabled2:this.data.dailyTask.task2button=='已完成'?true:false,
    })
  },
  // 去完成发起签到
  publishRecord:async function(){
    if(wx.getStorageSync('userinfo').type == 1){
      wx.navigateTo({
        url: '../teacher/sign_launch',
      })
    }else{
      await wx.User.userTypeAlter({
        data:{
          uid:wx.getStorageSync('userinfo').uid,
          type:1
        }
      })
    }
  },
  // 参与签到
  joinRecord:async function(){
    // 教师身份
    if(wx.getStorageSync('userinfo').type == 1){
      await wx.User.userTypeAlter({
        data:{
          uid:wx.getStorageSync('userinfo').uid,
          type:2
        }
      })
    }else{ //学生身份
      wx.reLaunch({
        url: '../student/home',
      })
      // wx.redirectTo({
      //   url: '../student/home',
      // })
    }
  },
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})