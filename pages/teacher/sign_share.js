const app = new getApp();
Page({
 
  /**
   * 页面的初始数据
   */
  data: {
    // 点击链接进入小程序显示支付按钮
    paymentShow: false,  
  },
 
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.warn("分享出去获取到的数据===>", options.id);
    if (options.id=="123"){
      this.setData({
        paymentShow: true
      })
    }
 
  },
  // onShareAppMessage: function () {
  //   return {
  //     title: '自定义分享标题',
  //     path: '/page/user?id=123'
  //   }
  // },
 
 
  // 分享
  onShareAppMessage: function(res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
    }

    // 将当前页面的路径包括参数使用encodeURIComponent进行编码并作为url的参数,path设置为小程序的首页.当我们将该页面
    // 分享给其他人时,他们打开的首先是首页,然后跳转到我们分享的这个页面.
    let url = encodeURIComponent('/pages/student/home?sign_id=' + 1);
    let sign_id = '1';

    return {
      title: '测试',
      path:`/pages/guide/guide?sign_id=${sign_id}&url=${url}`
    }
  },
})
