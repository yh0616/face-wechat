// pages/about/about.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
   appVersion:''
  },
//重置密码
onLoad: function (options) {
    const accountInfo = wx.getAccountInfoSync();
    this.setData({
    appVersion:"1.2.9",
    })
    console.log(accountInfo.miniProgram.version);

},
  resetPassword: function () {
    wx.navigateTo({
      url: './resetPsd',
    })
  },
  //帮助
  toHelp: async function () {
    wx.navigateTo({
      url: '../help/help',
    })

  },
  //意见反馈
  feedback: function () {
    wx.navigateTo({
      url: '../feedback/feedback',
    })
  },
  //隐私政策
  privacyPolicy:function(){
    wx.navigateTo({
      url: '../privacy/privacy',
    })
  }
})