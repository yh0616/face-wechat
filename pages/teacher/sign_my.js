const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userinfo: wx.getStorageSync('userinfo'),
    infoArray: {},
    reward_points: '',
    name: '',
    school: '',
    academy: '',
    avatarInfo: '',
    nickName: '',
    result: '',
    custom: (wx.User.token == null || wx.User.token == ''),
    codeResult: ''
  },
  onLoad: function (options) {
    app.editTabbar();
    this.setData({
      custom: (wx.User.token == null || wx.User.token == '')
    })
    if (wx.User.token == null || wx.User.token == '') {

    } else {
      wx.User.rewardPoints()
      this.data.infoArray.reward_points = wx.getStorageSync('userinfo').reward_points
      this.setData({
        userinfo: wx.getStorageSync('userinfo'),
        infoArray: this.data.infoArray,
      })
    }

  },
  onShow: async function () {
    this.setData({
      custom: (wx.User.token == null || wx.User.token == '')
    })
    if (wx.User.token == null || wx.User.token == '') {} else {
      await wx.User.rewardPoints()
      this.data.infoArray.reward_points = wx.getStorageSync('userinfo').reward_points
      this.setData({
        userinfo: wx.getStorageSync('userinfo'),
        reward_points: wx.getStorageSync('userinfo').reward_points
      })
    }

  },
  handleList: function (e) {
    wx.navigateTo({
      url: './sign_list',
    })
  },


  // 扫一扫登录网页
  scanLogin: function (e) {
    var _this = this;
    wx.scanCode({
      success: async (res) => {
        _this.setData({
          codeResult: res.result
        })
        if (res.result.substring(0, 15) == 'www.xqzjgsu.xyz') {
          await wx.User.waitConfirmation({
            data: {
              uid: wx.getStorageSync('userinfo').uid,
              sessionId: res.result.substring(26)
            }
          })
          if (wx.getStorageSync('scanLogin') == 200) {
            wx.showModal({
              title: '提示',
              content: "确认登录享签网页后端？", //提示内容
              confirmColor: '#0ABF53', //确定按钮的颜色
              success(res) {
                if (res.confirm) {
                  wx.User.getScanLogin({
                    data: {
                      uid: wx.getStorageSync('userinfo').uid,
                      sessionId: _this.data.codeResult.substring(26)
                    }
                  })
                } else if (res.cancel) {
                }
              }
            })
          } else {
            wx.showToast({
              title: '二维码已失效！',
              icon: 'none'
            })
          }
        } else if (res.result.substring(0, 2) == 'ey') {
          await _this.changeType()
          wx.User.scanCode({
            data: {
              code: res.result
            }
          })
        } else {
          wx.showToast({
            title: '无法识别的二维码！',
            icon: 'none'
          })
        }

      }
    })
  },
  copyText: function (e) {
    wx.setClipboardData({
      data: e.currentTarget.dataset.text,
      success: function (res) {
        wx.getClipboardData({
          success: function (res) {
            wx.showToast({
              icon: 'none',
              title: '复制成功'
            })
          }
        })
      }
    })
  },
  getUserProfile(e) {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认
    // 开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '用于获取用户头像', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        if (res.userInfo != null) {
          wx.setStorageSync('avatarInfo', res.userInfo)
          app.globalData.userInfo = res.userInfo
          wx.login({
            success: res => {
              var code = res.code
              new Promise(async (resolve, reject) => {
                await wx.User.login({
                  url: "/auth/login",
                  data: {
                    code: code,
                    userAvatar: app.globalData.userInfo.avatarUrl,
                    userAlias:wx.getStorageSync('avatarInfo').nickName
                  }
                })
                resolve(wx.getStorageSync('userinfo').type)
              }).then(res => {
                if (res == 1) {
                  wx.switchTab({
                    url: '/pages/teacher/sign_home',
                  })
                } else if (res == 2) {
                  wx.redirectTo({
                    url: '/pages/student/home',
                  })
                } else {
                  wx.navigateTo({
                    url: '/pages/index/index',
                  })
                }
                wx.hideLoading({
                  success: (res) => {},
                })
              })
            }
          })
        }
      }
    })
  },
  choseImage: function () {
    this.openAlert('头像暂不支持修改')
  },
  openAlert: function (e) {
    // wx.showToast({
    //   title: e,
    //   icon: "none"
    // })
  },
  showList: function () {
    wx.navigateTo({
      url: './sign_classList',
    })
  },
  // 跳转到修改信息页面
  updateUserInfo: function () {
    wx.navigateTo({
      url: '../teacher/sign_changeInfo',
    })
  },
  about: function () {
    wx.navigateTo({
      url: './about',
    })
  },

  // 切换身份
  changeType: async function () {
    await wx.User.userTypeAlter({
      data: {
        uid: wx.getStorageSync('userinfo').uid,
        type: 2
      }
    })
  },
  


  // 积分商城
  recordShop: function () {
    wx.navigateTo({
      url: '../products/products?reward_points=' + this.data.infoArray.reward_points,
    })
  },
  // 每日任务
  onDailyTask: function () {
    wx.navigateTo({
      url: '../dailyTask/dailyTask',
    })
  },
  

})