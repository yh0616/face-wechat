const app = getApp()
Page({
  /**
   * 页面的初始数据
   */
  data: {
    avatar: '',
    userinfo: wx.getStorageSync('userinfo'),
    // 个人信息
    name: '',
    university: '',
    academy: '',
    Class: '',
    address: '',
    age: '',
    email: '',
    major: '',
    phone: '',
    sex: '',
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    await wx.User.getUserInfo()
    // 获取从我的页面传过来的对象
    this.setData({
      avatar: wx.getStorageSync('avatarInfo').avatarUrl,
      userinfo: wx.getStorageSync('userinfo'),
      infoArray: wx.getStorageSync('userinfo'),
      name: wx.getStorageSync('userinfo').name,
      university: wx.getStorageSync('userinfo').university,
      academy: wx.getStorageSync('userinfo').academy,
      Class: wx.getStorageSync('userinfo').Class,
      address: wx.getStorageSync('userinfo').address,
      age: wx.getStorageSync('userinfo').age,
      email: wx.getStorageSync('userinfo').email,
      major: wx.getStorageSync('userinfo').major,
      phone: wx.getStorageSync('userinfo').phone,
      sex: wx.getStorageSync('userinfo').sex,
    })

  },
  // 提取输入框中的数字部分
  validateNumber(val) {
    if(Number(val)==0){
      return ' '
    }
    else{
    return val.replace(/\D/g, '')}
  },
  nameIn: function (e) {
    this.setData({
      name: e.detail.value
    })
  },
  universityIn: function (e) {
    this.setData({
      university: e.detail.value
    })
  },
  academyIn: function (e) {
    this.setData({
      academy: e.detail.value
    })
  },
  ageIn: function (e) {

    this.setData({
      age: this.validateNumber(e.detail.value)
    })
    if (this.data.age == '') {
      wx.showToast({
        title: '请正确输入',
        icon: 'none'
      })
    }
  },
  sexIn: function (e) {
    this.setData({
      sex: e.detail.value
    })
  },
  majorIn: function (e) {
    this.setData({
      major: e.detail.value
    })
  },
  ClassIn: function (e) {
    this.setData({
      Class: e.detail.value
    })
  },
  addressIn: function (e) {
    this.setData({
      address: e.detail.value
    })
  },
  phoneIn: function (e) {
    this.setData({
      phone: this.validateNumber(e.detail.value)
    })
    if (this.data.phone == '') {
      wx.showToast({
        title: '请输入11位数字',
        icon: 'none'
      })
    }
  },
  emailIn: function (e) {
    if ((/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}$/.test(e.detail.value))){
      this.setData({
        email: e.detail.value
      })
    }
    else{
          wx.showToast({
          title: '邮箱输入有误',
          duration: 1500,
          icon: 'none'
          });}
  },
  // 提交修改信息
  onSubmit: async function () {
    new Promise(async (resolve, reject) => {
      await wx.User.updateUserInfo({
        data: {
          uid: this.data.userinfo.uid,
          name: this.data.name,
          type: this.data.userinfo.type,
          age:parseInt(this.data.age),
          university: this.data.university,
          academy: this.data.academy,
          uclass: this.data.Class,
          address: this.data.address,
          email: this.data.email,
          major: this.data.major,
          phone: this.data.phone,
          sex: this.data.sex,
          avatar: this.data.avatar
        }
      })
      resolve(wx.getStorageSync('userinfo'))
    }).then(res => {
      setTimeout(function () {
        wx.navigateBack({
          delta: 1,
        })
      }, 1000)
    })
  },
  goBinding: function () {
    wx.navigateTo({
      url: './binding',
    })

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: async function () {
    await wx.User.getUserInfo()
    this.setData({
      userinfo: wx.getStorageSync('userinfo'),
      infoArray: wx.getStorageSync('userinfo'),
      // infoArray: JSON.parse(options.info)
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})