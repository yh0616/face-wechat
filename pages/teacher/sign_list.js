// pages/teacher/sign_list.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    listdata: [], //数据
    moredata: '',
    pageNum: 0, //当前分页；默认第一页
    pageSize: 10,
    index:0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      listdata: [],
    })
    this.loadmore();
  },
  // 点击显示删除签到活动弹框
  showDeleteDialog(e) {
    this.setData({
      actId: e.currentTarget.dataset.cid,
      modalName: e.currentTarget.dataset.target,
      index:e.currentTarget.dataset.index
    })
    
  },
  // 隐藏删除签到活动弹框
  hideDeleteDialog(e) {
    this.setData({
      modalName: null
    })
  },
  // 删除签到活动
  deleteActList: async function (e) {
    await wx.User.deleteActivity({
      data: {
        actid: this.data.actId
      }
    })
    this.hideDeleteDialog()
    var i = 0;
    var list=[];
    for(i; i<this.data.listdata.length;i++){
      if(i!=this.data.index){
        list.push(this.data.listdata[i])
      }
    }
    this.setData({
      listdata:list
    })
    if(this.data.listdata.length<5){
      this.onReachBottom()
    }
  },

  //触底事件
  onReachBottom: function () {
    var that = this;
    //检查是否还有数据可以加载
    var moredata = that.data.moredata;
    //如果还有，则继续加载
    if (moredata.length != 0) {
      this.loadmore();
    } else {
      //如果没有了，则停止加载，显示没有更多内容了
      wx.showToast({
        title: '没有更多了~',
        icon:'none'
      })
    }
  },
  //发起请求
  loadmore: async function () {
    //加载提示
    wx.showLoading({
      title: '加载中',
    })
    var that = this;
    //页面累加
    ++that.data.pageNum;
    await wx.User.findActByTid({
      data: {
        tid: wx.getStorageSync('userinfo').uid,
        pageSize: that.data.pageSize,
        pageNum: that.data.pageNum
      }
    })
    //隐藏加载提示
    wx.hideLoading();
    if (wx.getStorageSync('findAct').code == 200) {
      //判断数据是否为空
      if (wx.getStorageSync('findAct').data != []) {
        that.setData({
          //把新加载的数据追加到原有的数组
          listdata: that.data.listdata.concat(wx.getStorageSync('findAct').data), //加载数据
          moredata: wx.getStorageSync('findAct').data,
          pageSize: that.data.pageSize,
          pageNum: that.data.pageNum
        })
      } else {
      }
    }
  },
  
  // 点击查看签到活动的详情
  showSignInfo: function (e) {
    this.setData({
      actId: e.currentTarget.dataset.actid,
      lid: e.currentTarget.dataset.lid,
    })
    wx.navigateTo({
      url: '/pages/teacher/sign_result?actId=' + this.data.actId + '&lid=' + this.data.lid,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})