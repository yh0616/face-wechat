// pages/teacher/binding.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bindId:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      bindId: wx.getStorageSync('userinfo').bindId==0?'':wx.getStorageSync('userinfo').bindId,
    })
  },

  bindId: function (e) {
    this.data.bindId = e.detail.value
  },

  async confirmBinding(){
    if(this.data.bindId>2100000000)
    wx.showToast({
      title: "id格式有误",
      icon: "none"
    })
    else{
      await wx.User.bindId({
        data: {
          "uid": wx.getStorageSync('userinfo').uid,
          "bindId": this.data.bindId
        }
      })
      setTimeout(
        function () {
          wx.navigateBack({
            delta: 1,
          })
        },
        1500)
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})