// pages/teacher/sign_code.js
var QRCode = require('../../utils/weapp-qrcode');
var qrcode;

const W = wx.getSystemInfoSync().windowWidth;
const rate = 750.0 / W;

// const code_w = 300 / rate;
const code_w = 500 / rate;

const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 这个由后端传递参数，包括此次签到的主键信息
    // 为实现实时更新，要定时更新这个参数
    // 可以加上时间进行加密处理
    text: 'http://www.baidu.com/',
    image: '',
    code_w: code_w,
    actId: '',
    lid: '',
    endTime: '',
    code: '',

    className: '',
    title: '',
    linkType: null,
    ScreenBrightness:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    this.setData({
      actId: options.actId,
      lid: options.lid,
      title: options.title,
      linkType: options.linkType,
      className: options.className,
      status: options.status,
      endTime: options.endTime,
      cid: options.cid,
    })
    await this.getUpdateCode()
    qrcode = new QRCode('canvas', {
      text: this.data.code,
      image: '/images/default_head_circle.png',
      width: code_w,
      height: code_w,
      colorDark: "#222",
      colorLight: "white",
      correctLevel: QRCode.CorrectLevel.H,
    });

    var times = 0
    var that = this
    var i = setInterval(async function () {
      times++
      if (times >= 8) {
        await that.getUpdateCode()
        qrcode.makeCode(that.data.code)
        times = 0
      }
    }, 1000)

    this.setData({
      timer: i
    })
  },

  // 扫一扫登录网页
  scanLogin: function (e) {
    var _this = this;
    wx.scanCode({
      success: async (res) => {
        _this.setData({
          codeResult: res.result
        })
        if (res.result.substring(0, 15) == 'www.xqzjgsu.xyz') {
          await wx.User.waitConfirmation({
            data: {
              uid: wx.getStorageSync('userinfo').uid,
              sessionId: res.result.substring(26),
              type: 'codeLogin'
            }
          })
          if (wx.getStorageSync('scanLogin') == 200) {
            wx.showModal({
              title: '提示',
              content: "确认登录享签网页后端？", //提示内容
              confirmColor: '#0ABF53', //确定按钮的颜色
              success(res) {
                if (res.confirm) {
                  wx.User.getScanLogin({
                    data: {
                      uid: wx.getStorageSync('userinfo').uid,
                      sessionId: _this.data.codeResult.substring(26),
                      lid: _this.data.lid,
                      state: _this.data.status,
                      endTime: _this.data.endTime,
                      actId: _this.data.actId,
                      title: _this.data.title,
                      linkType: _this.data.linkType,
                      className: _this.data.className,
                      cid: _this.data.cid,
                      type: 'codeLogin'
                    }
                  })
                  console.log("dayin",_this.data.endTime)
                } else if (res.cancel) {
                }
              }
            })
          } else {
            wx.showToast({
              title: '二维码已失效！',
              icon: 'none'
            })
          }
        } else {
          wx.showToast({
            title: '无法识别的二维码！',
            icon: 'none'
          })
        }

      }
    })
  },

  // 实时获取二维码数据
  async getUpdateCode(options = {}) {
    // 更新数据
    const {
      data: res
    } = await wx.$http.get('/act/updateCode', {
      lid: this.data.lid
    }, {
      token: true,
      checkToken: true
    })
    // 失败
    if (res.code != 200) {
      wx.showToast({
        title: "获取信息失败",
        icon: "none"
      })
      return false;
    } else {
      this.setData({
        code: res.data.code
      })
    }
  },

  confirmHandler: function (e) {
    var value = e.detail.value
    qrcode.makeCode(value)
  },
  inputHandler: function (e) {
    var value = e.detail.value
    this.setData({
      text: value
    })
  },
  tapHandler: function () {
    // 传入字符串生成qrcode
    qrcode.makeCode(this.data.text)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  setScancode: function () {
    wx.request({
      url: 'https://cli.im/api/qrcode/code?text=' + 222 + '&mhid=sELPDFnok80gPHovKdI',
    })
  },


  // 扫码
  getScancode: function () {
    var _this = this;
    wx.scanCode({
      data: {
        code: _this.data.code
      },
      success: (res) => {
        wx.navigateTo({
          url: '../index/index?title=' + res.result
        })
        var result = res.result;
        _this.setData({
          result: result,
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this
    wx.getScreenBrightness({
      success: function (res) {
        that.setData({
          ScreenBrightness: res.value
        })
      }
    })
    console.log("屏幕亮度",this.data.ScreenBrightness)
    if (wx.setScreenBrightness) {
      wx.setScreenBrightness({
        value: 0.8
      });
      wx.setKeepScreenOn({
        keepScreenOn: true
      });
    } else {
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    clearInterval(this.data.timer);
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    var that = this;
    clearInterval(this.data.timer);
    if (wx.setScreenBrightness) {
      wx.setScreenBrightness({
        value: that.data.ScreenBrightness,
      })
      // 保持屏幕常亮 true / false
      wx.setKeepScreenOn({
        keepScreenOn: false
      });
    } else {
    }
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})