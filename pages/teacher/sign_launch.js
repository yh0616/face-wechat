// pages/teacher/sign_launch.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // screenHeight: 0,

    types: ["上课签到", ],
    typeslist: [],
    typeIndex: "0",
    titleHistory: [],
    captionHistory: [],
    showTitleHistory: false,
    showCaptionHistory: false,
    hasTwo: true,
    currentIndex: 0,
    point: false,

    activities: ["签到", "签退"],
    activityIndex: "0",

    title: '',
    desc: '',

    timelist: [1, 2, 3, 4, 5, 10, 15, 20, 25, 30],
    timeIndex: "0",
    // time: '00:00',

    classList: {},

    lists: [],
    listIndex: "0",

    signlist: [{
        duration: 5,
        endTime: "2020-10-26 14:29:00",
        linkType: 1,
        location_x: 0,
        location_y: 0,
        range: 0,
        startTime: "2020-10-26 14:29:00",
        status: 0,
      },
      {
        duration: 5,
        endTime: "2020-10-26 14:29:00",
        linkType: 2,
        location_x: 0,
        location_y: 0,
        range: 0,
        startTime: "2020-10-26 14:29:00",
        status: 0,
      }
    ],

    pubdata: {},

    userinfo: {},

    listflag: true,

    pointflag: [
      false,
      false
    ],
    choosen: {
      latitude: 0,
      longitude: 0
    },
    flag1: false,

    scopelist: [300, 400, 500, 800, 1000, 1500, 2000, 2500, 3000],
    scopeIndex: 2,

    navConfig: {
      title: '确认框',
      type: 1,
      config: {
        show: true,
        type: 'confirm',
        showTitle: true,
        title: '标题',
        content: '这个是确认框',
        confirmText: '确定',
        confirmColor: '#3683d6',
        cancelText: '取消',
        cancelColor: '#999'
      }
    },
    firstTip: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.data.userinfo = wx.User.userinfo
    if (wx.getStorageSync('titleHistory') != '')
      this.setData({
        titleHistory: wx.getStorageSync('titleHistory')
      })
    if (wx.getStorageSync('captionHistory') != '')
      this.setData({
        captionHistory: wx.getStorageSync('captionHistory')
      })
  },

  onShow: function () {
    this.data.userinfo = wx.User.userinfo
    this.getList()
    this.getTypeList()

  },

  titleInput(e) {
    if (e.type == "focus") {
      this.setData({
        showTitleHistory: true
      })
    }
    if (e.type == "blur") {
      this.setData({
        showTitleHistory: false
      })
      this.data.title = e.detail.value
      setTimeout(() => {
        var list = this.data.titleHistory
        if (e.detail.value != "") {
          for (var i = 0; i < list.length; i++) {
            if (list[i] == this.data.title) {
              list.splice(i, 1)
              list.splice(0, 0, this.data.title)
              this.setData({
                titleHistory: list
              })
              wx.setStorageSync('titleHistory', list)
              return;
            }
          }
          if (list.length == 5) {
            list.splice(4, 1)
          }
          list.splice(0, 0, this.data.title)
        }
        this.setData({
          showCaptionHistory: false,
          titleHistory: list
        })
        wx.setStorageSync('titleHistory', list)
      }, 500)
    }
  },

  captionInput(e) {
    if (e.type == "focus") {
      this.setData({
        showCaptionHistory: true
      })
    }
    if (e.type == "blur") {
      this.setData({
        showCaptionHistory: false
      })
      this.data.desc = e.detail.value
      setTimeout(() => {
        var list = this.data.captionHistory
        if (e.detail.value != "") {
          for (var i = 0; i < list.length; i++) {
            if (list[i] == this.data.desc) {
              list.splice(i, 1)
              list.splice(0, 0, this.data.desc)
              this.setData({
                captionHistory: list
              })
              wx.setStorageSync('captionHistory', list)
              return;
            }
          }
          if (list.length == 5) {
            list.splice(4, 1)
          }
          list.splice(0, 0, this.data.desc)
        }
        this.setData({
          showCaptionHistory: false,
          captionHistory: list
        })
        wx.setStorageSync('captionHistory', list)
      }, 500)
    }
  },

  titleTap(e) {
    var index = e.currentTarget.dataset.index
    this.data.title = this.data.titleHistory[index]
    this.setData({
      title: this.data.title
    })
  },

  captionTap(e) {
    var index = e.currentTarget.dataset.index
    this.data.desc = this.data.captionHistory[index]
    this.setData({
      desc: this.data.desc
    })
  },

  // 定位单选框点击
  clisk(e) {
    var tmp = this.data.pointflag
    tmp[e.currentTarget.dataset.index] = !tmp[e.currentTarget.dataset.index]
    if (tmp[e.currentTarget.dataset.index]) {
      this.setData({
        pointflag: tmp,
        currentIndex: e.currentTarget.dataset.index,
        modalName: e.currentTarget.dataset.target
      })
    } else {
      var link = this.data.signlist;
      link[e.currentTarget.dataset.index].location_x = 0;
      link[e.currentTarget.dataset.index].location_y = 0;
      link[e.currentTarget.dataset.index].range = 0;
      this.setData({
        pointflag: tmp,
        currentIndex: e.currentTarget.dataset.index,
        signlist: link
      })
    }
  },

  locationDetermine() {
    var index = this.data.currentIndex
    var location_x = 0
    var location_y = 0
    var range = 0
    if (this.data.pointflag[index]) {
      location_x = this.data.choosen.latitude
      location_y = this.data.choosen.longitude
      range = this.data.scopelist[this.data.scopeIndex]
      if (this.data.choosen.latitude == 0 && this.data.choosen.longitude == 0) {
        wx.showToast({
          title: '请选择位置！',
          icon: 'none'
        })
      } else {
        this.data.signlist[index].location_x = location_x;
        this.data.signlist[index].location_y = location_y;
        this.data.signlist[index].range = range;
        this.setData({
          signlist: this.data.signlist
        })
        this.hideModal()
      }
    } else {
      this.data.signlist[index].location_x = location_x;
      this.data.signlist[index].location_y = location_y;
      this.data.signlist[index].range = range;
      this.setData({
        signlist: this.data.signlist
      })
      this.hideModal()
    }
  },

  tapAdd(e) {
    var index = e.currentTarget.dataset.index
    if (this.data.signlist[index].duration == 30)
      return;
    if (this.data.signlist[index].duration >= 5) {
      this.data.signlist[index].duration += 5;
    } else {
      this.data.signlist[index].duration++
    }
    this.setData({
      signlist: this.data.signlist
    })
  },

  tapMin(e) {
    var index = e.currentTarget.dataset.index
    if (this.data.signlist[index].duration == 1)
      return;
    if (this.data.signlist[index].duration > 5) {
      this.data.signlist[index].duration -= 5;
    } else {
      this.data.signlist[index].duration--
    }
    this.setData({
      signlist: this.data.signlist
    })
  },

  toggle(e) {
    var anmiaton = e.currentTarget.dataset.class;
    var that = this;
    that.setData({
      animation: anmiaton
    })
    setTimeout(function () {
      that.setData({
        animation: ''
      })
    }, 1000)
  },

  async getList() {
    const {
      data: res
    } = await wx.$http.get('/class/findClassListByTid', {
      tid: this.data.userinfo.uid
    }, {
      token: true,
      checkToken: true
    })
    if (res.code != 200) {
      wx.showToast({
        title: "获取名单失败",
        icon: "none"
      })
    } else {
      if (res.data.length == 0) {
        wx.showModal({
          title: '提示',
          content: '请先去我的新增名单',
          success(res) {
            if (res.confirm) {
              wx.navigateTo({
                url: '/pages/teacher/sign_classList',
              })
            } else if (res.cancel) {}
          }
        })
        return
      }
      this.setData({
        classList: res.data
      })



      var lists = []
      this.data.classList.forEach(element => {
        lists.push(element.name)
      });
      this.setData({
        lists: lists
      })
      wx.setStorageSync('classList', res.data)
    }
  },

  async getTypeList() {
    const {
      data: res
    } = await wx.$http.get('/type/list', {}, {
      token: true,
      checkToken: true
    })
    if (res.code != 200) {
      wx.showToast({
        title: "获取类型名单失败",
        icon: "none"
      })
    } else {
      var types = []
      this.setData({
        typeslist: res.data
      })
      this.data.typeslist.forEach(element => {
        types.push(element.name)
      });
      this.setData({
        types: types
      })
      wx.setStorageSync('typesList', res.data)
    }
  },

  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },

  cancel: function () {
    this.hideModal()
  },

  //改变是否选择定位
  listenerSwitch: function (e) {
    this.setData({
      point: e.detail.value
    })
  },


  // 确定按钮
  onConfirmTap() {},

  // 取消按钮
  onCancelTap() {},

  // 显示 dio
  onShowDioTap() {
    const type = 1;
    const config = JSON.parse(JSON.stringify(this.data.navConfig.config));
    this.setData({
      currentConf: config,
      type
    });
  },


  //改变是否开启名单
  listenerSwitch2: function (e) {
    this.setData({
      listflag: e.detail.value,
      pointflag: [
        false,
        false
      ],
    })
    if (this.data.listflag == false && this.data.firstTip) {
      var navTemp = this.data.navConfig
      navTemp.config.content = "不选择名单，签到结果将不显示未签到成员，且无法开启定位"
      this.setData({
        navConfig: navTemp,
        firstTip: false
      })
      this.onShowDioTap()
    }
  },

  bindScopeChange(e) {
    this.setData({
      scopeIndex: e.detail.value
    })
  },

  chooseLocation: function () {
    // let that = this
    wx.chooseLocation({
      type: 'gcj02',
      success: (res) => {
        this.setData({
          choosen: res,
          flag1: true
        })
      },
    })
  },

  // 新增环节确定按钮
  determine: function () {
    var location_x = 0
    var location_y = 0
    var range = 0

    if (this.data.pointflag == true) {
      location_x = this.data.choosen.latitude
      location_y = this.data.choosen.longitude
      range = this.data.scopelist[this.data.scopeIndex]
      if (this.data.choosen.latitude == 0 && this.data.choosen.longitude == 0) {
        wx.showToast({
          title: '请选择位置！',
          icon: 'none'
        })
      } else {
        this.data.pointflag.push(this.data.point)
        this.setData({
          pointflag: this.data.pointflag
        })
        this.data.signlist.push({
          linkType: Number(this.data.activityIndex) + 1,
          duration: this.data.timelist[this.data.timeIndex],
          startTime: "2020-10-26 14:29:00",
          endTime: "2020-10-26 14:29:00",
          status: 0,
          location_x: location_x,
          location_y: location_y,
          range: range
        })
        this.setData({
          signlist: this.data.signlist
        })
        this.hideModal()
      }
    } else {
      this.data.pointflag.push(this.data.point)
      this.setData({
        pointflag: this.data.pointflag
      })
      this.data.signlist.push({
        linkType: Number(this.data.activityIndex) + 1,
        duration: this.data.timelist[this.data.timeIndex],
        startTime: "2020-10-26 14:29:00",
        endTime: "2020-10-26 14:29:00",
        status: 0,
        location_x: location_x,
        location_y: location_y,
        range: range
      })
      this.setData({
        signlist: this.data.signlist
      })
      this.hideModal()
    }
    if (this.data.signlist.length == 2) {
      this.setData({
        hasTwo: true
      })
    }
  },

  delete(e) {
    this.data.pointflag.splice(e.currentTarget.dataset.index, 1)
    this.data.signlist.splice(e.currentTarget.dataset.index, 1)
    this.setData({
      signlist: this.data.signlist,
      pointflag: this.data.pointflag,
      hasTwo: false
    })
  },

  //改变签到类型
  bindTypeChange: function (e) {
    this.setData({
      typeIndex: e.detail.value
    })
  },

  //改变名单选择
  bindListChange: function (e) {
    this.setData({
      listIndex: e.detail.value
    })
  },

  locationCancel() {
    var tmp = this.data.pointflag
    tmp[this.data.currentIndex] = !tmp[this.data.currentIndex]
    this.setData({
      pointflag: tmp,
      modalName: null
    })
  },

  //改变签到环节
  bindActivityChange: function (e) {
    this.setData({
      activityIndex: e.detail.value
    })
  },

  bindTimeChange(e) {
    this.setData({
      timeIndex: e.detail.value
    })
  },

  // 
  signLaunch: function (e) {
    var pubdata = this.data.pubdata
    pubdata.title = e.detail.value.title
    pubdata.actType = Number(this.data.typeIndex) + 1
    pubdata.caption = e.detail.value.desc
    if (this.data.listflag) {
      pubdata.cid = this.data.classList[this.data.listIndex].cid || ''
    }
    pubdata.tid = this.data.userinfo.uid
    pubdata.listSession = this.data.signlist


    if (pubdata.title == '' || pubdata.cid == '' || pubdata.listSession.length == 0) {
      wx.showModal({
        title: '提示',
        content: '请填写完整',
        success(res) {}
      })
      return
    }
    // 将所有的time转换成秒
    pubdata.listSession.forEach(element => {
      // var h = element.duration.split(':')
      var time = Number(element.duration)
      element['duration'] = time
    });
    // pubdata.listSession = this.data.signlist

    this.data.pubdata = pubdata

    wx.User.pubNewAct({
      data: pubdata
    })
  },

  __formatPubData(provider, options) {
    return {
      title: provider,
      actType: 0,
      caption: options.authResult.expires_in,
      cid: options.userInfo.nickName,
      tid: this.data.userinfo.uid,
      listSession: options
    }
  },

  __formatListSession(obj) {
    return {
      linkType: obj.linkType,
      duration: this.data.time,
      startTime: obj.startTime,
      endTime: obj.endTime,
      status: obj.status
    }
  },
})