const app = getApp();
var shareTicket, that, sessionKey, encryptedData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    className: '',
    userinfo: wx.getStorageSync('userinfo'),
    classList: '',
    name: '',
    count: '',
    changeName: '',
    changeCount: ''
  },
  nameIn: function (e) {
  //   if(e.detail.value.length>=0&&e.detail.value.length<21){
  //     this.data.infoArray.name = e.detail.value
  //   }
  //   else{
  //     wx.showToast({
  //       title: '名称输入错误',
  //       icon:'loading'
  //     })
  //   }
  // },
  // numIn: function (e) {
  //   if(Number(e.detail.value)>0&&Number(e.detail.value)<250){
  //     this.data.infoArray.count = e.detail.value
  //   }
  //   else{
  //     wx.showToast({
  //       title: '人数输入错误',
  //       icon:'loading'
  //     })
  //   }
    this.data.name = e.detail.value
  },
  numIn: function (e) {
    if(!(/^[0-9]{0,3}$/.test(e.detail.value))){
      wx.showToast({
        title: '请输入数字',
        icon: 'none',    //如果要纯文本，不要icon，将值设为'none'
        duration: 1000    //持续时间为 2秒
      }) 
      this.setData({
        count: ''
      })
    }else {
      this.setData({
        count: e.detail.value
      })
    }
  },
  // 提取输入框中的数字部分
  // validateNumber(val) {
  //   return val.replace(/\D/g, '')
  // },
  // 显示删除提示弹框
  showDeleteClass(e) {
    this.setData({
      cid: e.target.dataset.cid,
      modalName: e.currentTarget.dataset.target
    })
  },
  hideDeleteDialog(e) {
    this.setData({
      modalName: null
    })
  },
  // 点击显示签到名单成员
  onShowList: function (e) {
    var cid = e.currentTarget.dataset.cid
    var name = e.currentTarget.dataset.name
    wx.navigateTo({
      url: './classListDetail?cid=' + cid + '&name=' + name,
    })
  },
  // 删除班级名单
  deleteClassList: async function (e) {
    await wx.User.deleteClass({
      data: {
        cid: this.data.cid
      }
    })
    this.setData({
      modalName: null
    })
    this.onShow()
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.User.findClassListByTid()
    this.setData({
      classList: wx.getStorageSync('classList'),
      userinfo: wx.getStorageSync('userinfo')
    })
    shareTicket = app.shareTicket;
    /*
     * withShareTicket:true
     * 被打开的时候，可以获取一些信息，例如：群标识   
     * onLaunch / onShow  获取到shareTicket
     */
    wx.showShareMenu({
      withShareTicket: true,
      success: (res) => {
      },
      fail: () => {
      }
    })
  },
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  changeNum: function (e) {
    if(!(/^[0-9]{0,3}$/.test(e.detail.value))){
      wx.showToast({
        title: '请输入数字',
        icon: 'none',    //如果要纯文本，不要icon，将值设为'none'
        duration: 1000    //持续时间为 2秒
      }) 
      // this.setData({
      //   count: ''
      // })
    }else {
      this.setData({
        changeCount: e.detail.value
      })
    }
    // this.setData({
    //   changeCount:this.validateNumber(e.detail.value)
    // })
    // if(this.data.changeCount==''){
    //   wx.showToast({
    //     title: '请输入数字！',
    //     icon:'none',
    //     duration:1000
    //   })
    // }
  },
  // 弹出对应的签到名单的信息框
  changeClassNum(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target,
      changeName: e.currentTarget.dataset.name,
      changeCount: e.currentTarget.dataset.count,
      cid: e.currentTarget.dataset.cid
    })
  },
  // 隐藏修改签到名单弹框
  hidechangeClassNum(e) {
    this.setData({
      modalName: null
    })
  },
  // 修改名单人数
  changeClassCount: async function () {
    await wx.User.updateClassTCount({
      data: {
        cid: this.data.cid,
        t_count: this.data.changeCount
      }
    })
    await wx.User.findClassListByTid()
    this.setData({
      classList: wx.getStorageSync('classList'),
    })
    this.setData({
      modalName: null
    })
  },
  // 新增签到名单
  createClass: async function () {
    if (this.data.name == '' || this.data.count == '') {
      wx.showToast({
        title: '信息请填写完整！',
        icon: 'none'
      })
    } else {
      await wx.User.createClass({
        url: "/class/createClass",
        data: {
          tid: this.data.userinfo.uid,
          name: this.data.name,
          t_count: this.data.count
        }
      })
      this.setData({
        modalName: null
      })
      // 将表单中的值置空
      this.data.name = '',
        this.data.count = ''
      this.setData({
        name:this.data.name,
        count:this.data.count
      })
    }
    this.onShow()
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    var cid = res.target.dataset.cid;
    if (res.from === 'button') {
    } else if (res.from === 'menu') {
    }
    return {
      title: '欢迎加入 ' + wx.getStorageSync('userinfo').name + ' 的签到名单', // 转发标题
      path: '/pages/share/sharePage?cid=' + cid, // 当前页面 path ，必须是以 / 开头的完整路径
      imageUrl: 'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=1205629261,1517975709&fm=26&gp=0.jpg'
    }
  },
  onLaunch: function (e) {
    if (e.scene == 1044) {
    }
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: async function (res) {
    await wx.User.findClassListByTid()
    this.setData({
      classList: wx.getStorageSync('classList')
    })
  },
  /**
   * getOpenGid 获取群标识openGId
   */
  getOpenGid: function () {
    that = this;
    wx.getShareInfo({
      shareTicket: shareTicket,
      success: (res) => {
      },
      fail: (error) => {
      }
    })
  },

  // ListTouch触摸开始
  ListTouchStart(e) {
    this.setData({
      ListTouchStart: e.touches[0].pageX
    })
  },

  // ListTouch计算方向
  ListTouchMove(e) {
    this.setData({
      ListTouchDirection: e.touches[0].pageX - this.data.ListTouchStart > 0 ? 'right' : 'left'
    })
  },

  // ListTouch计算滚动
  ListTouchEnd(e) {
    if (this.data.ListTouchDirection == 'left') {
      this.setData({
        modalName: e.currentTarget.dataset.target
      })
    } else {
      this.setData({
        modalName: null
      })
    }
    this.setData({
      ListTouchDirection: null
    })
  },
})