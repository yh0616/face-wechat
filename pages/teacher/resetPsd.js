// pages/teacher/resetPsd.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    newPass: '',
    newPassSecond: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  newPassIn: function (e) {
    this.data.newPass = e.detail.value
  },
  newPassSecondIn: function (e) {
    this.data.newPassSecond = e.detail.value
  },
  submit:async function () {
    if (this.data.newPass == '' || this.data.newPassSecond == '') {
      wx.showToast({
        title: '请输入完整！',
        icon: 'none'
      })
    } else if (this.data.newPass != this.data.newPassSecond) {
      wx.showToast({
        title: '两次密码输入不一致！',
        icon: 'none'
      })
    } else {
      await wx.User.resetPwd({
        data: {
          "uid1": wx.getStorageSync('userinfo').uid1,
          "password": this.data.newPass
        }
      })
      setTimeout(function() {
        wx.navigateBack({
          delta: 0,
        })
      }, 500);
      
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})