// pages/signin/signin.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    motto: '获取定位',
    choosen: {
      latitude: 0,
      longitude: 0
    },
    got: {
      latitude: 0,
      longitude: 0
    },
    flag1: false,
    flag2: false
  },

  chooseLocation: function () {
    // let that = this
    wx.chooseLocation({
      type: 'gcj02',
      success: (res) => {
        // console.log(res)
        this.setData({
          choosen: res,
          flag1: true
        })
      },
    })
  },

  getLocation: function () {
    wx.getLocation({
      type: 'gcj02',
      success: (res) => {
        const latitude = res.latitude
        const longitude = res.longitude
        wx.openLocation({
          latitude,
          longitude,
          scale: 18
        })
        this.setData({
          got: res,
          flag2: true
        })
      },
    })
  },

  Rad: function (d) {
    return d * Math.PI / 180.0;
  },

  calculate: function () {
    let lat1 = this.data.choosen.latitude
    let lat2 = this.data.got.latitude
    let lng1 = this.data.choosen.longitude
    let lng2 = this.data.got.longitude
    var radLat1 = this.Rad(lat1);
    var radLat2 = this.Rad(lat2);
    var a = radLat1 - radLat2;
    var b = this.Rad(lng1) - this.Rad(lng2);
    var s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));
    s = s * 6378137.0; // 取WGS84标准参考椭球中的地球长半径(单位:m)
    s = Math.round(s * 10000) / 10000;
    s = s.toFixed(2)
    this.setData({
      motto: s + 'm'
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    this.getLocation()
    var choosen = {
      latitude: options.location_x,
      longitude: options.location_y
    }
    this.setData({
      lid: options.lid,
      choosen: choosen,
      range: options.rangement,
      flag1: true
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})