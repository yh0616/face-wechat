const app = getApp()
Page({
  data: {
    screenHeight: 0,
    newUser: false,
    screenHeight1: 0,
    filter: 0,
    secretDis: 'none',
    userinfo: '',
  },

  onLoad: async function (e) {
    var that = this
    let token = wx.User.token || '';
    this.setData({
      newuser:(token == '' ? true : false)
    })

    // if (wx.getStorageSync('tabBar') == '') {
    //   wx.User.getTabBar()
    // }
    if (wx.getStorageSync('avatarInfo') != '') {
      wx.login({
        success: res => {
          wx.User.login({
            url: "/auth/login",
            data: {
              code: res.code,
              userAvatar: wx.getStorageSync('avatarInfo').avatarUrl,
              userAlias:wx.getStorageSync('avatarInfo').nickName
            }
          })
          setTimeout(
            function () {
              // if (wx.getStorageSync('tabBar') == '') {
              //   wx.User.getTabBar()
              // }
              if (wx.getStorageSync('userinfo').type == 1) {
                wx.switchTab({
                  url: '/pages/teacher/sign_home',
                })
              } else if (wx.getStorageSync('userinfo').type == 2) {
                wx.redirectTo({
                  url: '/pages/student/home',
                })
              } else {
                wx.redirectTo({
                  url: '/pages/index/index',
                })
              }
            },
            1500)
        }
      })
    } else {
      // 新用户
      this.setData({
        newUser: true
      })
    }
    this.setData({
      screenHeight: wx.getSystemInfoSync().windowHeight,
      screenHeight1: wx.getSystemInfoSync().windowHeight,
    }, () => {
      var k = that;
      setTimeout(function () {
        that.setData({
          screenHeight1: k.data.screenHeight * 0.66,
        })
      }, 500)
    })
  },
  toHome: function (e) {
    wx.switchTab({
      url: '../teacher/sign_home',
    })
  },
})