const app = getApp()
Page({
  data: {
    userInfo: {},
    hasUserInfo: false,
    //判断小程序的API，回调，参数，组件等是否在当前版本可用
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    score_stu: '#F8F8F8',
    score_tea: '#F8F8F8',
    userinfo: ''
  },
  
  score_stu: function (e) {
    //点击按钮，样式改变
    let that = this;
    that.setData({
      score_stu: '#0ABF53'
    });
  },
  score_tea: function (e) {
    //点击按钮，样式改变
    let that = this;
    that.setData({
      score_tea: '#0ABF53'
    });
  },
  onLoad: function () {
    this.data.userinfo = wx.User.userinfo;
    this.setData({
      userinfo:wx.User.userinfo
    })
    var that = this;
    if (that.data.userinfo.type == 1) {
      wx.switchTab({
        url: '/pages/teacher/sign_home',
      })
    } else if (that.data.userinfo.type == 2) {
      wx.redirectTo({
        url: '/pages/student/home',
      })
    }
  },
  onShow: function () {
    wx.hideHomeButton();
  },

  getStuUserInfo:async function() {
    var that = this
    this.setData({
      userInfo: app.globalData.userInfo,
      hasUserInfo: true
    })
    console.log("点击getStuUserInfo==>",that.data.userinfo)
    var uid = that.data.userinfo.uid
    console.log("uid==>",uid)
    wx.request({
      url: 'https://www.xqzjgsu.xyz/fss/auth/userTypeAlter',
      data:{
        uid:uid,
        type:2
      },
      success:function(res){
        console.log("回调",res)
      }
    })
    wx.redirectTo({
      url: '../register/register',
    })
  },

  getTeaUserInfo:async function () {
    var that = this
    this.setData({
      userInfo: app.globalData.userInfo,
      hasUserInfo: true
    })
    var uid = that.data.userInfo.uid
    wx.request({
      url: 'https://www.xqzjgsu.xyz/fss/auth/userTypeAlter',
      data:{
        uid:uid,
        type:1
      },
      success:function(res){
        console.log("回调",res)
      }
    })
    wx.switchTab({
      url: '../teacher/sign_home',
    })
  }
})