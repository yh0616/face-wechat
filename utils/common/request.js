import config from  "./config.js";
import User from "./user.js";
import myrequest from "./api.js";
export default {
  config: {
    baseUrl:config.webUrl,
    header: {
      'Content-Type':'application/json;charset=UTF-8',
			// 'Content-Type':'application/x-www-form-urlencoded'
    },
    data: {},
		method: "GET",
		dataType: "json",
  },
  request(options = {}) {
    options.header = options.header || this.config.header;
    options.method = options.method || this.config.method;
		options.dataType = options.dataType || this.config.dataType;
    options.url = this.config.baseUrl+options.url;
    // 这里的token是true or false
    if(options.token) {
      // 验证用户是否登陆
      // 没登录就不请求，直接返回
      if(!this.checkToken(options.checkToken)) return;
      // 登录了就将token放到header上
      options.header.Authorization = 'Bearer ' + User.token;
    }
    return myrequest(options);
  },
  get(url,data,options={}) {
    options.url = url;
    options.data = data;
    options.method = 'GET';
    return this.request(options);
  },
  post(url,data,options={}){
		options.url = url;
		options.data = data;
    options.method = 'POST';
		return this.request(options);
  },
  // 验证用户是否登录
	checkToken(checkToken){
		if (checkToken && !User.token) {
			// 未登录
			// wx.showToast({
      //   title: '请先登录',
      // });
			// wx.navigateTo({
      //   url: '/pages/index/index',
      // });
			return false;
		}
		return true;
	},
}