import $http from "./request.js";
export default {
	// 用户token 测试token：4cd36bf70649475ac0cd6fae78250954474a4350
	token: false,
	// 用户信息
	userinfo: false,
	// 初始化
	__init() {
		// 获取用户信息
		this.userinfo = wx.getStorageSync("userinfo");
		this.token = wx.getStorageSync("token");
	},
	// 权限验证跳转
	// 类似路由守卫
	navigate(options, NoCheck = false, type = "navigateTo") {
		// 是否登录验证
		// $http使用了request.js的方法
		if (!$http.checkToken(true)) return;
		// 跳转
		switch (type) {
			case "navigateTo":
				wx.navigateTo(options);
				break;
			case "redirectTo":
				wx.redirectTo(options);
				break;
			case "reLaunch":
				wx.reLaunch(options);
				break;
			case "switchTab":
				wx.switchTab(options);
				break;
		}
	},
	// 登录
	async login(options = {}) {
		// 请求登录
		const {
			data: res
		} = await $http.post(options.url, options.data)
		// 登录失败
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
			return false;
		}
		// 登录成功 保存状态
		// Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJxdWxpdGFvIiwiY3JlYXRlZCI6MTU4NjY4NTQxNzk3NywiZXhwIjoxNTg3MjkwMjE3fQ.1B0umjdawHeqFfAQeNNQLAKEbkXjH0jr8o9PjCw2sfAwWdQlLSOQRCPX2w79mFD8_aFajZBkekS3Io9WSMPDTA
		this.token = res.data.token;
		this.userinfo = res.data;
		// 本地存储
		wx.setStorageSync("userinfo", this.userinfo);
		wx.setStorageSync("token", this.token);
		return true;
	},
	// 退出登录
	async logout(showToast = true) {
		// 退出登录
		// await $http.post('/user/logout',{},{ 
		// 	token:true,
		// 	checkToken:true ,
		// });
		// 清除缓存
		wx.removeStorageSync('userinfo');
		wx.removeStorageSync('token');
		// 清除状态
		this.token = false;
		this.userinfo = false;
		// 返回home页面
		wx.switchTab({
			url: "/pages/index/index",
			success() {
				let page = getCurrentPages().pop(); //跳转页面成功之后
				if (!page) return;
				page.onLoad(); //如果页面存在，则重新刷新页面
			}
		})

		// 退出成功
		if (showToast) {
			return wx.showToast({
				title: '退出登录成功'
			});
		}
	},

	// 获取用户相关信息
	async getUserInfo() {
		const {
			data: res
		} = await $http.post('/auth/selectUserById', {
			uid: this.userinfo.uid
		}, {
			token: true,
			checkToken: true
		})
		if (res.code == 200) {
			this.userinfo.name = res.data.name
			this.userinfo.type = res.data.type
			this.userinfo.academy = res.data.academy
			this.userinfo.university = res.data.university
			this.userinfo.major = res.data.major
			this.userinfo.Class = res.data.class
			this.userinfo.age = res.data.age
			this.userinfo.phone = res.data.phone
			this.userinfo.email = res.data.email
			this.userinfo.sex = res.data.sex
			this.userinfo.address = res.data.address
			this.userinfo.uid1 = res.data.uid1
			this.userinfo.bindId = res.data.bindid
			this.userinfo.reward_points = res.data.reward_points
			wx.setStorageSync('userinfo', this.userinfo)
		} else if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
	},
	// 扫码登录
	async getScanLogin(options = {}) {
		const {
			data: res
		} = await $http.get('/auth/scanCodeLogin', options.data, {
			token: true,
			checkToken: true
		})
		console.log("扫码", res)
		if (res.code == 200) {
			wx.showToast({
				title: '扫码登录成功',
				icon: 'success'
			})
		}
	},


	// 扫码登录确认
	async waitConfirmation(options = {}) {
		const {
			data: res
		} = await $http.get('/auth/waitConfirmation', options.data, {
			token: true,
			checkToken: true
		})
		console.log("扫码登录确认", res)
		wx.setStorageSync('scanLogin', res.code)
	},

	// 获取TabBar
	// async getTabBar() {
	// 	const {
	// 		data: res
	// 	} = await $http.get('/auth/getTabBar')

	// 	if (res.message == "token过期，请重新登录！") {
	// 		wx.User.token = ''
	// 		wx.switchTab({
	// 			url: '../../pages/teacher/sign_my',
	// 		})
	// 	}
	// 	if (res.code != 200) {
	// 		wx.showToast({
	// 			title: res.message,
	// 			icon: "none"
	// 		})
	// 	} else {
	// 		wx.setStorageSync('tabBar', res.data)
	// 	}
	// },

	// 更新用户信息
	async updateUserInfo(options = {}) {
		// 更新数据
		const {
			data: res
		} = await $http.post('/auth/updateUserById', options.data, {
			token: true,
			checkToken: true
		})

		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		// 失败
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
			return false;
		} else {
			await this.getUserInfo()
			wx.showToast({
				title: res.message,
				icon: "success"
			})
		}
	},

	// 增加名单
	async createClass(options = {}) {
		const {
			data: res
		} = await $http.get(options.url, options.data, {
			token: true,
			checkToken: true
		})
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else {
			wx.showToast({
				title: res.message,
				icon: "success"
			})
		}
	},

	//获取历史名单
	async classListHistory(options = {}) {
		const {
			data: res
		} = await $http.get('/class/classListHistory', options.data, {
			token: true,
			checkToken: true
		})
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else {
			wx.setStorageSync('classFile', res.data)
		}
	},

	//删除历史名单
	async deleteListFile(options = {}) {
		const {
			data: res
		} = await $http.get('/class/deleteListFile', options.data, {
			token: true,
			checkToken: true
		})
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		}
	},

	// 对比名单
	async contrast(options = {}) {
		const {
			data: res
		} = await $http.get('/rec/contrast', options.data, {
			token: true,
			checkToken: true
		})
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		}
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		} else {
			wx.setStorageSync('unSignList', res.data)
			wx.showToast({
				title: "对比成功",
				icon: "sucess"
			})
		}
	},

	// 查询教师名单
	async findClassListByTid() {
		const {
			data: res
		} = await $http.get('/class/findClassListByTid', {
			tid: this.userinfo.uid
		}, {
			token: true,
			checkToken: true
		})
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else {
			wx.setStorageSync('classList', res.data)
		}
	},

	// 查询教师签到列表
	async findActByTid(options = {}) {
		const {
			data: res
		} = await $http.get('/act/findActByTid', options.data, {
			token: true,
			checkToken: true
		})
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		console.log(res.data)
		wx.setStorageSync('findAct', res)
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else {
			console.log(res.data);
			wx.setStorageSync('actList', res.data)
		}
	},

	// 查询月份中有签到活动的日期
	async selectActivityByMonth(options = {}) {
		const {
			data: res
		} = await $http.get('/act/selectActivityByMonth', options.data, {
			token: true,
			checkToken: true
		})
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else {
			console.log("dateList", res.data)
			wx.setStorageSync('dateList', res.data)
		}
	},


	// 学生查询月份中有签到活动的日期
	async selectRecordByMonth(options = {}) {
		const {
			data: res
		} = await $http.get('/rec/selectRecordByMonth', options.data, {
			token: true,
			checkToken: true
		})
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else {
			console.log("stuDateList", res.data)
			wx.setStorageSync('stuDateList', res.data)
		}
	},

	// 加入签到名单
	async joinClass(options = {}) {
		const {
			data: res
		} = await $http.get(options.url,
			options.data, {
				token: true,
				checkToken: true
			})
		console.log(res)
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else {
			wx.showToast({
				title: res.message,
				icon: "success"
			})
		}
		setTimeout(
			function () {
				wx.redirectTo({
					url: '../../pages/guide/guide',
				})
			},
			1500)
	},

	// 发起签到
	async pubNewAct(options = {}) {
		const {
			data: res
		} = await $http.post('/act/pubNewAct', options.data, {
			token: true,
			checkToken: true
		})
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else {
			console.log("成功", res)
			wx.showToast({
				title: res.message,
				icon: "success"
			})
			setTimeout(
				function () {
					wx.switchTab({
						url: '/pages/teacher/sign_home',
					})
					wx.showToast({
						title: res.data,
						icon: 'none'
					})
				},
				500)
		}
	},

	// 修改签到名单人数
	async updateClassTCount(options = {}) {
		const {
			data: res
		} = await $http.get('/class/updateClassTCount', options.data, {
			token: true,
			checkToken: true
		})
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else {
			wx.showToast({
				title: res.message,
				icon: "success"
			})
		}
	},

	// 删除签到名单
	async deleteClass(options = {}) {
		const {
			data: res
		} = await $http.get('/class/deleteClass', options.data, {
			token: true,
			checkToken: true
		})
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else {
			wx.showToast({
				title: res.message,
				icon: "success"
			})
		}
	},

	// 更新签到名单环节状态
	async updateLinkStatus(options = {}) {
		const {
			data: res
		} = await $http.get('/act/updateLinkStatus', options.data, {
			token: true,
			checkToken: true
		})
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else {
			wx.showToast({
				title: res.message,
				icon: "success"
			})
			wx.setStorageSync('signtypedata', res.data)
		}
	},
	// 查询签到名单人员
	async selectUserList(options = {}) {
		const {
			data: res
		} = await $http.get('/class/selectUserList', options.data, {
			token: true,
			checkToken: true
		})
		console.log(res.code)
		if (res.code == 199) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
			wx.setStorageSync('classListPer', '')
		} else if (res.code == 200) {
			wx.setStorageSync('classListPer', res.data)
			console.log("list_user", wx.getStorageSync('classListPer'))
		} else if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		} else {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		}
	},
	// 删除签到活动
	async deleteActivity(options = {}) {
		const {
			data: res
		} = await $http.get('/act/deleteActivity', options.data, {
			token: true,
			checkToken: true
		})
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else {
			wx.showToast({
				title: res.message,
				icon: "success"
			})
		}
	},

	// 删除名单某人员
	async deleteUserInClass(options = {}) {
		const {
			data: res
		} = await $http.get('/class/deleteUserInClass', options.data, {
			token: true,
			checkToken: true
		})
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else {
			wx.showToast({
				title: res.message,
				icon: "success"
			})
		}
	},
	// 扫描二维码
	async scanCode(options = {}) {
		const {
			data: res
		} = await $http.get('/act/scanCode', options.data, {
			token: true,
			checkToken: true
		})
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code == 141) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else if (res.code == 200) {
			wx.showToast({
				title: res.message,
				icon: "success"
			})
			console.log("扫码签到", res)
			wx.setStorageSync('lid', res.data)
			wx.navigateTo({
				url: '../../pages/student/scan_faceActive',
			})
		} else {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		}
	},
	// 新增学生签到记录
	async addNewRecord(options = {}) {
		const {
			data: res
		} = await $http.get('/rec/addNewRecord', options.data, {
			token: true,
			checkToken: true
		})
		console.log("scanCode", res)
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		wx.setStorageSync('addNewCode', res)
	},
	// 新增人脸
	async addFace(options = {}) {
		const {
			data: res
		} = await $http.post('/auth/addFace', options.data, {
			token: true,
			checkToken: true
		})
		console.log("人脸新增", res)
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else {
			wx.showToast({
				title: res.message,
				icon: "success"
			})
			console.log(this.userinfo)
			wx.redirectTo({
				url: './home',
			})
		}
	},
	// 人脸对比相似度
	async compareFace(options = {}) {
		const {
			data: res
		} = await $http.post('/auth/compareFace', options.data, {
			token: true,
			checkToken: true
		})
		console.log("人脸对比", res)
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		wx.setStorageSync('faceCode', res.code)
		wx.setStorageSync('faceData', res.data)
		if (res.code == 183) {
			wx.setStorageSync('hintTitle', '请真人签到')
			// wx.showToast({
			// 	title: '活体检测不成功',
			// 	icon: "none"
			// })
		} else if (res.code == 200) {
			if (res.data > 0.8) {
				wx.setStorageSync('hintTitle', '人脸对比成功')
				wx.showToast({
					title: res.message,
					icon: "success"
				})
			} else {
				wx.setStorageSync('hintTitle', '请本人签到')
			}
		} else if (res.code == 187) {
			wx.setStorageSync('hintTitle', '请正视摄像头')
		} else if (res.code == 500) {
			wx.setStorageSync('hintTitle', res.message)
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else {
			wx.setStorageSync('hintTitle', '请先注册人脸')
		}
	},
	// 学生端查询签到记录
	async selectRecord(options = {}) {
		const {
			data: res
		} = await $http.get('/rec/selectRecord', options.data, {
			token: true,
			checkToken: true
		})
		console.log("学生端签到记录", res)
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else {
			wx.setStorageSync('recordList', res.data)
		}
	},

	// 查询我的待签到列表
	async selectMyRecord(options = {}) {
		const {
			data: res
		} = await $http.get('/rec/selectMyRecord', options.data, {
			token: true,
			checkToken: true
		})
		console.log("我的待签到列表", res)
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else {
			wx.setStorageSync('MyRecord', res.data)
		}
	},

	// 每日任务
	async dailyTask(options = {}) {
		const {
			data: res
		} = await $http.get('/auth/dailyTask', options.data, {
			token: true,
			checkToken: true
		})
		console.log("我的每日任务", res)
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else {
			wx.setStorageSync('dailyTask', res.data)
		}
	},

	// 切换身份
	async userTypeAlter(options = {}) {
		console.log("进入更改身份==>",options.data)
		const {
			data: res
		} = await $http.get('/auth/userTypeAlter', options.data, {
			token: true,
			checkToken: true
		})
		console.log("切换身份", res)
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else {
			if (res.data == null) {
				wx.switchTab({
					url: '/pages/teacher/sign_home',
				})
			} else if (res.data == "Registered") {
				wx.reLaunch({
					url: '/pages/student/home',
				})
			} else {
				wx.navigateTo({
					url: '/pages/student/recognition',
				})
			}
		}
	},

	// 查询商品
	async productsList() {
		const {
			data: res
		} = await $http.post('/pro/productsShowList', {
			token: true,
			checkToken: true
		})
		console.log("商城商品", res)
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else {
			wx.setStorageSync('productsList', res.data)
		}
	},

	// 兑换商品
	async redeemProduct(options = {}) {
		const {
			data: res
		} = await $http.post('/pro/redeemProduct', options.data, {
			token: true,
			checkToken: true
		})
		console.log("兑换商品", res)
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else {
			wx.showToast({
				title: res.message,
				icon: "success"
			})
		}
	},
	// 兑换商品
	async redeemRecordListMy(options = {}) {
		const {
			data: res
		} = await $http.get('/pro/redeemRecordListMy', {
			uid: this.userinfo.uid
		}, {
			token: true,
			checkToken: true
		})
		console.log("兑换商品记录", res)
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else {
			wx.setStorageSync('redeemRecordList', res.data)
			wx.showToast({
				title: res.message,
				icon: "success"
			})
		}
	},

	// 教师代签
	async recByTea(options = {}) {
		const {
			data: res
		} = await $http.get('/rec/recByTea', options.data, {
			token: true,
			checkToken: true
		})
		console.log("代签结果", res)
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		wx.setStorageSync('recByTeaCode', res.code)
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else {
			wx.setStorageSync('recByTea', res.message)
			// wx.showToast({
			// 	title: res.message,
			// })
		}
	},

	//签到状态改变
	async setSignInState(options = {}) {
		const {
			data: res
		} = await $http.get('/rec/setSignInState', options.data, {
			token: true,
			checkToken: true
		})
		console.log("结果", res)
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else {
			// wx.showToast({
			// 	title: res.message,
			// })
		}
	},

	// 加为好友
	async joinFriend(options = {}) {
		const {
			data: res
		} = await $http.get('/friend/join',
			options.data, {
				token: true,
				checkToken: true
			})
		console.log(res)
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else {
			wx.showToast({
				title: res.message,
				icon: "success"
			})
			setTimeout(
				function () {
					wx.redirectTo({
						url: '../../pages/guide/guide',
					})
				},
				1500)
		}
	},

	// 查询好友排行榜
	async selectFriendRank(options = {}) {
		const {
			data: res
		} = await $http.get('/friend/order', options.data, {
			token: true,
			checkToken: true
		})
		console.log(res.code)
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else if (res.code == 200) {
			wx.setStorageSync('friendRank', res)
			// wx.showToast({
			// 	title: "获取成功",
			// 	icon: "success"
			// })
		}
	},


	// 查询签到总排行榜
	async selectFriendTotalRank(options = {}) {
		const {
			data: res
		} = await $http.get('/friend/rank', options.data, {
			token: true,
			checkToken: true
		})
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		console.log("查询签到总榜", res)
		wx.setStorageSync('friendTotalRank', res)
	},
	
	// 查询积分总排行榜
	async selectscoreRank(options = {}) {
		const {
			data: res
		} = await $http.get('/friend/scoreRank', options.data, {
			token: true,
			checkToken: true
		})
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		console.log("查询签到总榜", res)
		wx.setStorageSync('scoreTotalRank', res)
	},

	// 多人脸对比
	async facesComparison(options = {}) {
		const {
			data: res
		} = await $http.post('/auth/facesComparison', options.data, {
			token: true,
			checkToken: true
		})
		console.log("多人脸对比", res)
		wx.setStorageSync('facesComparisonCode', res.code)
		wx.setStorageSync('facesComparisonData', res.data)
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.setStorageSync('facesComparisonMessage', res.message)
			// wx.showToast({
			// 	title: '活体检测不成功',
			// 	icon: "none"
			// })
		} else if (res.code == 200) {
			wx.setStorageSync('facesComparisonMessage', res.message)
			// wx.showToast({
			// 	title: '多人脸对比成功！',
			// 	icon: "success"
			// })
		}
	},

	// 视频识别
	async videoFacesComparison(options = {}) {
		const {
			data: res
		} = await $http.post('/auth/videoFacesComparison', options.data, {
			token: true,
			checkToken: true
		})
		console.log("视频识别", res)
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		wx.setStorageSync('videoComparisonCode', res.code)
		wx.setStorageSync('videoComparisonData', res.data)
		if (res.code != 200) {
			wx.setStorageSync('videoComparisonMessage', res.message)
			// wx.showToast({
			// 	title: '活体检测不成功',
			// 	icon: "none"
			// })
		} else if (res.code == 200) {
			wx.setStorageSync('videoComparisonMessage', res.message)
			// wx.showToast({
			// 	title: '多人脸对比成功！',
			// 	icon: "success"
			// })
		}
	},

	// 发起签到者导出excel
	async excel(options = {}) {
		const {
			data: res
		} = await $http.get('/rec/excel', options.data, {
			token: true,
			checkToken: true
		})
		console.log("excel", res)
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: '获取导出失败',
				icon: "none"
			})
		} else if (res.code == 200) {
			wx.showToast({
				title: '导出成功',
				icon: "success"
			})
		}
	},

	// 检测人脸是否注册
	async checkFaceExist(options = {}) {
		const {
			data: res
		} = await $http.get('/auth/checkFaceExist', options.data, {
			token: true,
			checkToken: true
		})
		console.log(res.code)
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else if (res.code == 200) {
			wx.setStorageSync('checkFaceExist', res)
			// wx.showToast({
			// 	title: "获取成功",
			// 	icon: "success"
			// })
		}
	},


	// 重置密码
	async resetPwd(options = {}) {
		const {
			data: res
		} = await $http.post('/auth/resetPwd', options.data, {
			token: true,
			checkToken: true
		})
		console.log(res.code)
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else if (res.code == 200) {
			wx.showToast({
				title: res.message,
				icon: "success"
			})

		}
	},
	// 积分获取
	async rewardPoints() {
		const {
			data: res
		} = await $http.get('/auth/rewardPoints', {
			uid: this.userinfo.uid
		}, {
			token: true,
			checkToken: true
		})
		if (res.message == "token过期，请重新登录！") {
			wx.User.token = ''
			wx.switchTab({
				url: '../../pages/teacher/sign_my',
			})
		}
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else if (res.code == 200) {
			this.userinfo.reward_points = res.data
			wx.setStorageSync('userinfo', this.userinfo)

		}
	},
	// 重置密码
	async bindId(options = {}) {
		const {
			data: res
		} = await $http.get('/auth/bindId', options.data, {
			token: true,
			checkToken: true
		})
		if (res.code != 200) {
			wx.showToast({
				title: res.message,
				icon: "none"
			})
		} else if (res.code == 200) {
			wx.showToast({
				title: res.message,
				icon: "success"
			})
		}
	},



	// 检查是否可以更新人脸
	async canUpdateFace(options = {}) {
		const {
			data: res
		} = await $http.get('/auth/canUpdateFace', options.data, {
			token: true,
			checkToken: true
		})
		wx.setStorageSync('checkData', res)
	},

}