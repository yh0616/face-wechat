//app.js
import request from './utils/common/request.js'
import user from './utils/common/user.js'
App({
  
  onLaunch: async function () {
    wx.getSystemInfo({
      success: e => {
        this.globalData.StatusBar = e.statusBarHeight;
        let custom = wx.getMenuButtonBoundingClientRect();
        this.globalData.Custom = custom;  
        this.globalData.CustomBar = custom.bottom + custom.top - e.statusBarHeight;
      }
    })


    // 挂载全局方法
    wx.$http = request
    wx.User = user
    
    wx.User.__init()
    
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 获取用户信息
    // wx.getSetting({
    //   success: res => {
    //     if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          // wx.login({
          //   success: res => {
              // 可以将 res 发送给后台解码出 unionId
              // console.log("res.userInfo"+res)

              // this.globalData.userInfo = res.userInfo
              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              // if (this.userInfoReadyCallback) {
              //   this.userInfoReadyCallback(res)
              // }
            // }
          // })
      //   }
      // }
    // })
  },
  globalData: {
    userInfo: null,
    loginflag: false,
    tabBar: {
      "backgroundColor": "#fff",
      "color": "#333333",
      "selectedColor": "#26C55E",
      // "list": wx.getStorageSync('tabBar')
      "list": 
      [
        {
          "pagePath": "/pages/teacher/sign_home",
          "text": "首页",
          "iconPath": "../../images/home1.png",
          "selectedIconPath": "../../images/home2.png"
        },
        {
          "pagePath": "/pages/teacher/sign_launch",
          "isSpecial": true,
          "text": "创建签到",
          "iconPath": "../../images/publish.png",
          "selectedIconPath": "../../images/publish.png",
        },
        {
          "pagePath": "/pages/teacher/sign_my",
          "text": "我的",
          "iconPath": "../../images/my1.png",
          "selectedIconPath": "../../images/my2.png"
        }
      ]
    }
  },
  editTabbar: function () {
    //隐藏系统tabbar
    wx.hideTabBar();
    // this.globalData.tabBar.list=wx.getStorageSync('tabBar');
    // this.globalData.tabBar.list=wx.getStorageSync('tabBar');
    let tabbar = this.globalData.tabBar;
    let currentPages = getCurrentPages();
    let _this = currentPages[currentPages.length - 1];
    let pagePath = _this.route;
    (pagePath.indexOf('/') != 0) && (pagePath = '/' + pagePath);
    for (let i in tabbar.list) {
      tabbar.list[i].selected = false;
      (tabbar.list[i].pagePath == pagePath) && (tabbar.list[i].selected = true);
    }

    _this.setData({
      tabbar: tabbar
    });
  },
})